﻿using System;
using System.Collections.Generic;

namespace Zadatak_3_4
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "C:\\users\\Rerna\\source\\repos\\LV7_Bruno_Šimunović\\text.txt";
            SystemDataProvider systemDataProvider = new SystemDataProvider();
            systemDataProvider.Attach(new ConsoleLogger());
            systemDataProvider.Attach(new FileLogger(path));

            while (true)
            {
                systemDataProvider.GetAvailableRAM();
                systemDataProvider.GetCPULoad();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
