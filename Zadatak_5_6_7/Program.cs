﻿using System;

namespace Zadatak_5_6_7
{
    public enum DVDType { SOFTWARE, MOVIE }
    class Program
    {
        static void Main(string[] args)
        {
            BuyVisitor buyVisitor = new BuyVisitor();
            Book book = new Book("Svijet Atlantide", 49.0);
            VHS vHS = new VHS("Pirates", 101.0);
            DVD dVD = new DVD("Windows 10 Home", DVDType.SOFTWARE, 550.0);

            Console.WriteLine(book.ToString());
            Console.WriteLine(dVD.ToString());
            Console.WriteLine(vHS.ToString());
            Console.WriteLine();
            Console.WriteLine(book.Accept(buyVisitor).ToString("#.##"));
            Console.WriteLine(dVD.Accept(buyVisitor).ToString("#.##"));
            Console.WriteLine(vHS.Accept(buyVisitor).ToString("#.##"));

            Console.WriteLine();
            Visitor visitor = new Visitor();
            Console.WriteLine(book.Accept(visitor).ToString("#.##"));
            Console.WriteLine(dVD.Accept(visitor).ToString("#.##"));
            Console.WriteLine(vHS.Accept(visitor).ToString("#.##"));

            Cart cart = new Cart();
            cart.AddItem(book);
            cart.AddItem(dVD);
            cart.AddItem(vHS);

            Console.WriteLine();
            Console.WriteLine(cart.Accept(visitor));

        }
    }
}
