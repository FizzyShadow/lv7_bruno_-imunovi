﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_5_6_7
{
    class Cart : IItem
    {
        List<IItem> items;

        public Cart()
        {
            items = new List<IItem>();
        }
        public void AddItem(IItem item)
        {
            items.Add(item);
        }

        public void RemoveItem(IItem item)
        {
            items.Remove(item);
        }

        public double Accept(IVisitor visitor) 
        {
            double bill = 0;
            foreach(IItem item in items)
            {
                bill += item.Accept(visitor);
            }
            return bill;
        }
    }
}
