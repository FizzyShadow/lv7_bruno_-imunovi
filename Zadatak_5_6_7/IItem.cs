﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_5_6_7
{
    interface IItem 
    { 
        double Accept(IVisitor visitor); 
    }
}
