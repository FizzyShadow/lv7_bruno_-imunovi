﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_5_6_7
{
    class Visitor : IVisitor
    {
        private const double LoanOfItem = 0.1;

        //Zadatk 6

        //public double Visit(DVD DVDItem)
        //{
        //    if(DVDItem.Type == DVDType.SOFTWARE)
        //    {
        //        return double.NaN;
        //    }
        //    else
        //    {
        //        return DVDItem.Price * (1 + LoanOfItem);
        //    }
        //}

         public double Visit(DVD DVDItem)
        {
            if(DVDItem.Type == DVDType.SOFTWARE)
            {
                return DVDItem.Price;
            }
            else
            {
                return DVDItem.Price * (1 + LoanOfItem);
            }
        }
        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * (1 + LoanOfItem);
        }
        public double Visit(Book BookItem)
        {
            return BookItem.Price * (1 + LoanOfItem);
        }

    }
}
