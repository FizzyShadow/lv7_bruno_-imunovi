﻿using System;
using System.Globalization;

namespace LV7_Bruno_Šimunović
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 1.0, 2.2, 3.3, 1.7, 5.4, 2.4, 4.4, 7.7, 4.3, 5.5, 0.9 };
            double[] array1 = { 1.0, 2.2, 3.3, 1.7, 5.4, 2.4, 4.4, 7.7, 4.3, 5.5, 0.9 };
            double[] array2 = { 1.0, 2.2, 3.3, 1.7, 5.4, 2.4, 4.4, 7.7, 4.3, 5.5, 0.9 };

            SortStrategy sortStrategyBubbleSort = new BubbleSort();
            SortStrategy sortStrategyCombSort = new CombSort();
            SortStrategy sortStrategySeqentialSort = new SequentialSort();
            SearchStrategy searchStrategyBinarySearch = new BinarySearch();
            SearchStrategy searchStrategySequentialSearch = new SequentialSearch();

            NumberSequence numberSequence = new NumberSequence(array);
            Console.WriteLine(numberSequence.ToString());
            numberSequence.SetSortStrategy(sortStrategyBubbleSort);
            numberSequence.SetSearchStrategy(searchStrategyBinarySearch);
            numberSequence.Sort();
            Console.WriteLine("Trazeni broj je na inedksu: " + numberSequence.Search(4.3));
            Console.WriteLine(numberSequence.ToString());

            numberSequence = new NumberSequence(array1);
            numberSequence.SetSortStrategy(sortStrategyCombSort);
            numberSequence.SetSearchStrategy(searchStrategyBinarySearch);
            numberSequence.Sort();
            Console.WriteLine("Trazeni broj je na inedksu: " + numberSequence.Search(3.3));
            Console.WriteLine(numberSequence.ToString());

            numberSequence = new NumberSequence(array2);
            numberSequence.SetSortStrategy(sortStrategySeqentialSort);
            numberSequence.SetSearchStrategy(searchStrategySequentialSearch);
            numberSequence.Sort();
            Console.WriteLine("Trazeni broj je na inedksu: " + numberSequence.Search(0.9));
            Console.WriteLine(numberSequence.ToString());
        }
    }
}
