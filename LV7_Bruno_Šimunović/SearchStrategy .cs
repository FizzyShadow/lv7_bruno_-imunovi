﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7_Bruno_Šimunović
{
    abstract class SearchStrategy 
    { 
        public abstract int Search(double[] array, double target); 
    }
}
